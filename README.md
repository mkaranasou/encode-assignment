#Assignment Description
-----------------------

Programming Assignment Estimated Duration: 3 - 5 days

"Write a command-line Python script that should take as input a datetime range, retrieve the StackOverflow answer data 
for the given time range from the StackExchange API ( https://api.stackexchange.com/docs/answers) and compute the following:

- Total number of accepted answers
- Average score for all the accepted answers
- Average answer count per question

For the top 10 answers, ordered by score descending, find the comment count for every answer (https://api.stackexchange.com/docs/comments-on-answers)
It is advised not to spend much time with error handling and edge cases, but instead focus on writing clear and well-documented code.

It is also fine to not complete every assignment step, just mention what steps you have covered in the delivered solution.

Bonus points (5): the script generates an HTML table containing the computed statistics using a Jinja2 template and saves the result to a file. 
The file path should also be a command-line parameter .”

#Implementation and How To
--------------------------------
###- Details:
   
   The script takes as input two dates in the following format YYYY-MM-dd, and an optional path. The path is used to create
   the generated html file with the results using Jinja2 templating engine. If a path is not provided, then an html file
   'output_file.html' is created in the ecode-assigment/data directory.
   
   The script uses the two dates and retrieves all the answer data for the requested range, going page by page, having the answers ordered by votes. 
   It does not check whether the range is improper, e.g. from date is after to date.
   
   When all data is gathered, it calculates the requested statistics
        - Total number of accepted answers
        - Average score for all the accepted answers
        - Average answer count per question
   and retrieves the relevant comment data for the top 10 answers, ordered by score descending.
   
   Lastly, it renders the data to the output.html template using jinja2 library.
   
      In case of a bad response (response code != 200), it informs the user and exits.

###- Installation:
   Requires Python 2.7+

   From the command line:
   
        cd /path/to/encode-assignment
        pip install requirements.txt
        # or if that fails:
        sudo pip install -U -r requirements.txt 

###- Usage:
   From the command line:
   
        python path/to/main.py --f 2016-07-01 --t 2016-07-02 --o full/path/to/file.html
        
###- Tests:
    Under the tests package there are some basic sanity tests regarding proper datetime conversion and other aspects of the module.

*The module has been tested in Unix environment only.*