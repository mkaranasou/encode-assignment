import os
import unittest
from datetime import datetime

from stackoverflow_crawler.main import valid_path, MODULE_PATH
from stackoverflow_crawler.models.options import Options
from stackoverflow_crawler.models.retriever import Retriever
from stackoverflow_crawler.utils.date_utils import convert_to_epoch


class RetrieverTests(unittest.TestCase):
    def test_date_conversion(self):
        self.assertEqual(convert_to_epoch(datetime.strptime("2016-07-01 00:00:00", "%Y-%m-%d %H:%M:%S")), 1467331200)

    def test_retriever(self):
        date_from = datetime.strptime("2016-07-01 00:00:00", "%Y-%m-%d %H:%M:%S")
        date_to = datetime.strptime("2016-07-02 00:00:00", "%Y-%m-%d %H:%M:%S")
        if os.path.exists("/Users/mariakaranasou/Projects/encode-assignment/data/output_3.html"):
            os.remove()
        retriever = Retriever(Options(date_from, date_to, "/Users/mariakaranasou/Projects/encode-assignment/data/output_3.html"), MODULE_PATH)
        retriever.retrieve()

        self.assertTrue(os.path.exists("/Users/mariakaranasou/Projects/encode-assignment/data/output_3.html"), True)

    def test_valid_path(self):
        self.assertRaises(Exception, valid_path, "/somepath")
        self.assertRaises(Exception, valid_path, "../data/output.html")

    def test_valid_path(self):
        self.assertEqual(valid_path(None), None)
        self.assertEqual(valid_path("/Users/mariakaranasou/Projects/encode-assignment/data/output_file.html"), "/Users/mariakaranasou/Projects/encode-assignment/data/output_file.html")
        self.assertRaises(Exception, valid_path, "/somepath")
        self.assertRaises(Exception, valid_path, "../data/output.html")