import os
import sys
import platform
# Extend path with module
print('Python %s on %s' % (sys.version, sys.platform))
MODULE_PATH = os.path.dirname(os.path.realpath(__file__)).replace("stackoverflow_crawler", "")
sys.path.extend([os.path.dirname(MODULE_PATH)])

PATH_SEPARATOR = "/"
if "Windows" in platform.system():
    PATH_SEPARATOR = "\\"

from datetime import datetime
from stackoverflow_crawler.models.options import Options
from stackoverflow_crawler.models.retriever import Retriever


def valid_date(s):
    """
    http://stackoverflow.com/questions/25470844/specify-format-for-input-arguments-argparse-python
    :param s:
    :return:
    """
    try:
        return datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def valid_path(s):
    """
    Checks if user input s is a valid path
    :param s: string
    :return: s
    """
    if s is None:
        return s

    if os.path.isdir(PATH_SEPARATOR.join(s.split(PATH_SEPARATOR)[:-1])):
        if PATH_SEPARATOR + "output.html" not in s:
            return s
        else:
            raise Exception("Cannot use this file as output")
    else:
        raise Exception("Path is not valid.")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Retrieve StackOverflow data and calculate stats')
    parser.add_argument('--f', help='YYYY-mm-dd : The START date of the date range for the data request.', type=valid_date)
    parser.add_argument('--t', help='YYYY-mm-dd : The END date of the date range for data request.', type=valid_date)
    parser.add_argument('--o', nargs='?', help='The path for the html output.', type=valid_path)

    args = parser.parse_args()

    try:
        retriever = Retriever(Options(args.f, args.t, args.o), MODULE_PATH)
        retriever.retrieve()
    except KeyboardInterrupt:
        print "Exiting..."