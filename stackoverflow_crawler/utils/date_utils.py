from datetime import datetime


def convert_to_epoch(date_time):
    """
    Converts a datetime object to unix epoch seconds representation
    :param date_time: datetime
    :return: integer representation of datetime in unix epoch (seconds)
    """
    epoch = datetime.utcfromtimestamp(0)
    return int((date_time - epoch).total_seconds())
