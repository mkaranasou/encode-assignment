from datetime import datetime
from stackoverflow_crawler.utils.date_utils import convert_to_epoch


class Options(object):
    """

    """
    def __init__(self, date_from, date_to, out_file):
        """

        :param date_from:
        :param date_to:
        :param out_file:
        """
        self.initial_date_from = date_from
        self.initial_date_to = date_to
        self.date_from = convert_to_epoch(datetime.strptime(str(self.initial_date_from), '%Y-%m-%d %H:%M:%S'))
        self.date_to = convert_to_epoch(datetime.strptime(str(self.initial_date_to), '%Y-%m-%d %H:%M:%S'))
        if out_file is None:
            self.out_file = '../data/output_file.html'
        else:
            self.out_file = out_file