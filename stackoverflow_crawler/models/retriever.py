import requests
import operator
from stackoverflow_crawler.models.stats import Stats
from jinja2 import Environment, FileSystemLoader


class Retriever(object):
    """

    """
    def __init__(self, options, module_path):
        """

        :param options:
        :param module_path:
        """
        self.options = options
        self.module_path = module_path
        self.ans_url = 'https://api.stackexchange.com/2.2/answers?page={page}&pagesize=100&' \
                       'fromdate={from_date}&todate={to_date}' \
                       '&order=desc&sort=votes&site=stackoverflow'
        self.comments_url = 'https://api.stackexchange.com/2.2/answers/{ans_id}/comments' \
                            '?page={page}&order=desc&sort=creation&site=stackoverflow'
        self.ans_items = []
        self.more = True
        self.stats = Stats()
        self.score_per_ans = None
        self.answers_per_q = None
        self.accepted_ans_score = 0
        self.sorted_answers = None
        self.page_count = 0

    def retrieve(self):
        """
        Orchestrates the retrieval of data and statistics calculation
        :return: None
        """

        self._initialize()

        # if something goes wrong here...
        if self._retrieve_answer_data():
            self._calculate_stats()

            # ... or here, we need to stop
            if self._request_comment_data():
                self._print_html_doc()
        else:
            # for back up and debugging purposes
            self._write_data()
        print "[X] Process Completed!"

    def _initialize(self):
        """
        Initializes all variables to defaults
        :return:None
        """
        self.score_per_ans = {}
        self.answers_per_q = {}
        self.accepted_ans_score = 0
        self.sorted_by_score_answers = {}
        self.more = True
        self.page_count = 0

    def _retrieve_answer_data(self):
        """
        Retrieves the answer data for the range requested
        :return:boolean True if success, False otherwise
        """
        return self._retrieve_data(self.ans_url,
                            self._store_answer_data,
                            from_date=self.options.date_from,
                            to_date=self.options.date_to)

    def _store_answer_data(self, data, **kwargs):
        """
        Stores incoming answer data for further use
        :param data:list of json objects
        :param kwargs:
        :return:None
        """
        self.ans_items += data['items']

    def _retrieve_data(self, url, callback, **kwargs):
        """
        Performs the requests to the url until all data have been retrieved
        :param url: the url of the request with named arguments
        :param callback: a function to be called when data is received and good to process
        :param kwargs:any arguments to be used in the url
        :return: boolean True if success, False otherwise
        """
        success = True
        self.page_count = 0
        self.more = True

        print "[X] Requesting data from url:", url.format(page=1, **kwargs)

        while self.more:
            self.page_count += 1
            print "[X] Retrieving page no.", self.page_count

            response = requests.get(url.format(page=self.page_count, **kwargs))
            data = response.json()

            if response.status_code == 200:
                if "error_message" not in data:
                    callback(data, **kwargs)
                    self.more = data["has_more"] == True
                else:
                    self.more = False
                    success = False
                    print "Bad response:{0}\n{1}".format(data["error_message"], data["error_name"])
            else:
                self.more = False
                success = False
                print "Bad response code:{0}\n{1}".format(response.status_code, response.json())

        return success

    def _calculate_stats(self):

        for each in self.ans_items:

            # keep a count of scores for each answer
            self._track_answer_score(each)

            # keep a count of answers per question
            self._track_answers_per_question(each)

            # keep count of total accepted answers and their scores
            self._track_count_of_total_accepted_answers(each)

        # get average score
        if self.stats.total_num_accepted_ans > 0:  # else average is zero
            self.stats.avg_score_for_all_accepted_ans = round(float(
                self.accepted_ans_score) / self.stats.total_num_accepted_ans, 1)

        if len(self.answers_per_q) > 0:
            self.stats.avg_ans_count_per_q = round(sum(self.answers_per_q.values()) / float(len(self.answers_per_q.keys())), 1)
            # todo: this can be avoided since answers are coming in sorted by votes
            self.sorted_by_score_answers = sorted(self.score_per_ans.items(), key=operator.itemgetter(1), reverse=True)

        self.stats.display()

    def _track_answer_score(self, answer_data):
        # keep a count of scores for each answer
        if answer_data['answer_id'] not in self.score_per_ans:
            self.score_per_ans[answer_data['answer_id']] = 0
            self.score_per_ans[answer_data['answer_id']] += answer_data['score']

    def _track_answers_per_question(self, answer_data):
        if answer_data['question_id'] not in self.answers_per_q:
            self.answers_per_q[answer_data['question_id']] = 0
        self.answers_per_q[answer_data['question_id']] += 1

    def _track_count_of_total_accepted_answers(self, answer_data):
        if answer_data['is_accepted']:
            self.stats.total_num_accepted_ans += 1
            self._track_score_of_total_accepted_answers(answer_data['score'])

    def _track_score_of_total_accepted_answers(self, score):
            self.accepted_ans_score += score

    def _print_html_doc(self):
        """
        Loads the output.html Jinja2 template and renders the statistics to the requested file path
        :return: None
        """

        # Create the jinja2 environment - trim_blocks helps control whitespace.
        j2_env = Environment(loader=FileSystemLoader(self.module_path + "/data"),
                             trim_blocks=True)
        # get template and render
        rendered_data = j2_env.get_template("output.html").render(
            title='Results from data retrieved from StackOverflow for: {0} - {1}'.format(self.options.initial_date_from.date(),
                                                                                         self.options.initial_date_to.date()),
            statistics=self._get_table_data(),
            table_headers=["Target", "#of comments"],
            comment_count=self._get_comment_count_table_data()
        )

        # write rendered data to disc
        with open(self.options.out_file, 'wb') as out_file:
            out_file.write(rendered_data)

        print "[X] Template rendered in:", self.options.out_file

    def _get_table_data(self):
        """
        Gathers statistics in a key value format suitable for the template : [(stat, value),... ]
        :return: A list of sets
        """
        table_data = {"Total number of accepted answers": self.stats.total_num_accepted_ans,
                      "Average score for all the accepted answers": self.stats.avg_score_for_all_accepted_ans,
                      "Average answer count per question": self.stats.avg_ans_count_per_q}

        return [(k, v) for k,v in table_data.items()]

    def _get_comment_count_table_data(self):
        """
        Gathers comment data in a format suitable for the template : [(answer, comment count),... ]
        :return: A list of sets
        """
        table_data = {}

        if self.sorted_by_score_answers is not None:
            for each in self.sorted_by_score_answers[:10]:
                table_data["Comment count for " + str(each[0]) + " with score:" + str(each[1])] = \
                self.stats.comment_data[each[0]]

        return [(k, v) for k,v in table_data.items()]

    def _write_data(self):
        """
        Writes answer data to file for debugging.
        :return:
        """
        with open('../data/demo_data.txt', 'wb') as out_file:
            for each in self.ans_items:
                out_file.write(str(each))

    def _request_comment_data(self):
        """
        For each answer id in the 10 first answers ordered by score, retrieve the relevant comments
        :return:boolean True if success, False otherwise
        """
        success = False

        for answer_id in self.sorted_by_score_answers[:10]:
            success = self._retrieve_data(self.comments_url, self._store_comment_data, ans_id=answer_id[0])

            # Check if something went wrong - which means we probably hit a limit -
            # and if yes, return to stop the procedure
            if not success:
                break

        return success

    def _store_comment_data(self, data, **kwargs):
        """
        Stores comment count for answer
        :param data: list of comment data
        :param kwargs:
        :return: None
        """
        answer_id = kwargs.get('ans_id', None)
        if answer_id not in self.stats.comment_data:
            self.stats.comment_data[answer_id] = 0
        self.stats.comment_data[answer_id] += len(data['items'])



