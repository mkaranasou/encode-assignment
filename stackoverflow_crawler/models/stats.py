class Stats(object):
    def __init__(self):
        self.total_num_accepted_ans = 0             # Total number of accepted answers
        self.avg_score_for_all_accepted_ans = 0     # Average score for all the accepted answers
        self.avg_ans_count_per_q = 0                # Average answer count per question
        self.comment_data = {}                      # The comment count for every answer in TOP 10 answers

    def display(self):
        print "# Statistics for request:                       #"
        print "# --------------------------------------------- #"
        print "# Total number of accepted answers:        {0}   #".format(self.total_num_accepted_ans)
        print "# Average score for all accepted answers:  {0}   #".format(self.avg_score_for_all_accepted_ans)
        print "# Average answer count per question:       {0}   #".format(self.avg_ans_count_per_q)
        for k, v in self.comment_data.items():
            print "# Comment count for {0}:                             {1}   #".format(k, v)
        print "# --------------------------------------------- #"
